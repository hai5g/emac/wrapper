;;; yasnippet.el ---                      -*- lexical-binding: nil; -*-
;;;
;;; Copyright (C) 2017  Cong Hai Nguyen
;;;
;;; Author: Cong Hai Nguyen
;;; Keywords: internal
;;; License: AGPLv3
;;;
;;; Commentary:
;;;
;;; Code:
;;;
;;; * Name
;;;
;;; h-yasnippet - a text-based snippet package built on top of
;;; `yasnippet'
;;;
;;; * Why h-yasnippet?
;;;
;;; - `yasnippet' (version 20180916.2115) does not have functionality to expand snippet's prefix
;;;  with "<tab>"
;;; - If there are multiple snippet with the same key, `yasnippet'
;;;  prompt will show a list of keys without distinguishing which
;;;  major-mode they belong to.
;;; - `yasnippet' does not recommend nested expansion and it
;;;  dependence on numeric value of argument
;;;
;;; * Limitations
;;;
;;; `yasnippet' gets error in statement like `(error "some message")`, it should be written as `(message "some message")` or `"some message"` instead.

(h-require 'yasnippet)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SET THIS BEFORE REQUIRE-ING 'yasnippet) ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; otherwise variable won't be updated by emacs
;;;
;;; This suppresses warning of expanding quoted expression (i.e. embeded code)
;;; inside snippet

(defvar warning-suppress-types nil)
(add-to-list 'warning-suppress-types '(yasnippet backquote-change))


(setq
 yas-triggers-in-field t ; (nil) ; enabling nested expansion
 yas-buffer-local-condition nil ; don't expand inside comment
 ;; yas-key-syntaxes '("w_" "w")

 ;; using fixed cause indentation problem for new_prog_mode (Created
 ;; tag get unwanted indent when script has `comment-region')) ; DONT
 ;; expand just partial match (e.g. buffer-<substring>):
 yas-indent-line 'auto ; (('fixed))
 ;; DONT expand just partial match (e.g. buffer-<substring>)
 yas-wrap-around-region 'nil)

(define-key yas-minor-mode-map (kbd "<f7> n") 'yas-visit-snippet-file)
(define-key yas-minor-mode-map (kbd "<f7> N") 'yas-new-snippet)


(defun h-yas-expand-snippet (n)
  "Vanilla yasnippet does not have function to expand snippet by its name. Hence this.
N: snippet base name"
  (yas-expand-snippet (yas-lookup-snippet n))

  ;; always return nil so that when used inside a snippet it won't
  ;; make yasnippet insert this "t" into the buffer:
  nil 
)


;;; TO DELETE IF NO PROBLEM

;;; ;; REPLACED BY a more functional `h-yasnippet' mode
;;; (defun h-yas-singleton-template (orig-fun &rest args)
;;;   "Workaround to avoid being asked for snippets with same key.
;;; Returns the snippet object of the nearest major-mode."
;;;   ;; (message "display-buffer called with args %S" args)
;;;   (let ((singleitem-templates (list (caar args))))
;;;     ;; (message "display-buffer returned %S" singleitem-templates)
;;;     (apply orig-fun (cons singleitem-templates (cdr args)))))
;;;   (advice-add 'yas--expand-or-prompt-for-template :around #'h-yas-singleton-template)
; 


;;;###autoload
(defun h-yas-expand (&optional major-mode-str)
  "Expand SNIP-BASENAME into its evaluated content.
SNIP-BASENAME: the base filename of the snippet.
MAJOR-MODE-STR: e.g. ''sh-mode''"
  (interactive (list (symbol-name major-mode)))
  (let (start end botap fullname snipbody
	      ;; found-p: need this to avoid killing snippet prefix
	      ;; prematurely by waiting until found snippet to kill
	      found-p 
	      )
    (if (region-active-p)
	(setq start (region-beginning)
	      end (region-end))
      (setq botap (bounds-of-thing-at-point 'symbol)))
    (setq found-p
	  (if (or (null botap)
		  (null (setq start (car botap)))
		  (null (setq end (point)))
		  (= start end)
		  ;; When point at middle of symbol -> user might want to
		  ;;use tab for something else, e.g. cycle expanding
		  ;;heading in org-mode, so do not expand snippet here:
		  (< end (cdr botap)))
	      nil
	    (setq fullname
		  (h-yas-get-snip-fullname
		   (buffer-substring-no-properties start end)
		   (or major-mode-str (symbol-name major-mode))))
	    (setq snipbody
		  (if fullname
		      (h-yas--parse-snippet-body fullname)
		    nil))
	    (if snipbody
		(progn
		  ;; (message "snippet found: %s" fullname)
		  (unless yas-minor-mode
		    (yas-minor-mode 1)
		    ;; (yas-reload-all)
		    )
		  (kill-region start end)
		  (yas-expand-snippet
		   snipbody
		   ;; ;; upstream bug by `yasnippet', sometimes snippet
		   ;; ;; not recognized even correctly identifying its
		   ;; ;; basename and major-mode and already turn on
		   ;; ;; `yas-minor-mode' and rerun `yas-reload-all'
		   ;; (h-yas-lookup-snippet fullname)
		   )
		  t)
	      nil)))
    (unless found-p
      (h-yas--fallback))))


(defun h-yas-get-snip-fullname (snip-basename major-mode-str)
  (cl-assert (not (null snip-basename)))
  (cl-assert (stringp snip-basename))
  (cl-assert (not (null major-mode-str)))
  (let ((snipfname
	 (h-yas-bfs-snipfname ; fetch snip fname from snip prefix
	  snip-basename
	  (list major-mode-str))))
    snipfname
    ))


(defun h-yas-bfs-snipfname (snip-prefix mode-name_stack)
  "Returns the full filename of the target snippet via breadth-first
search.

- MODE-NAME_STACK :: the stack of directory names
- SNIP-PREFIX :: prefix to be searched in the directory (historical
                 name, currently no longer search for snippet file's
                 prefix but simply search for its full basename)."
  (cl-assert (listp mode-name_stack))
  (let (
	(major-mode-name nil) ; base dir name (usually a major-mode name)
	(top-dir-name (car yas-snippet-dirs)) ; root snippet dir
	(major-mode-dir nil) ; = top-dir-name + snip-prefix
	(fullname nil) ; = major-mode-dir + snip-prefix
	(matched-list ())
        )    
    (while (and (null fullname) (> (length mode-name_stack) 0))
      (setq major-mode-name (pop mode-name_stack)
	    major-mode-dir (concat top-dir-name "/" major-mode-name "/")
	    fullname (concat major-mode-dir snip-prefix)
	    matched-list (cons
			  fullname
			  (file-expand-wildcards
			   (concat (replace-regexp-in-string "[*]" "[*]" fullname) "*"))))
      ;; (message "checking %s" fullname)
      ;; get the result immediately if prefix does match a snippet
      ;; basename instead of querying for all snippets with this
      ;; prefix
      (unless (file-exists-p fullname)
	(setq fullname
	      (pcase (length matched-list)
		(0  nil)
		;; ;; DONT hungrily expand even if exactly 1 snippet
		;; ;; has this prefix - always ask user to avoid
		;; ;; unwanted hungrily expansion:
		;; (1 (car matched-list))
		(_ (concat major-mode-dir
			   (completing-read
			    (format "Pick 1 snippet with this prefix (%s)?: " major-mode-name)
			    (mapcar 'file-name-nondirectory matched-list)
			    nil t snip-prefix))))))
      (unless (and (stringp fullname)
		   (file-exists-p fullname)) ; this lookup full filename
	(setq fullname nil)
	(setq mode-name_stack
	      (append mode-name_stack
		      (h-yas-parent-modes major-mode-dir)))))
    fullname))


(defun h-yas-parent-modes (start-dir)
  "Fetch parent dirs specified in START-DIR."
  (let ((parent-list-fname (concat start-dir "/" ".yas-parents"))
	(result-alist ()))
    (when (file-exists-p parent-list-fname)
      (with-temp-buffer
	(insert-file-contents parent-list-fname)
	(setq
	 result-alist
	 (split-string
	  (buffer-substring-no-properties (point-min) (point-max))
	  "[\n\r]" t
	  ))))
    result-alist))


(defun h-yas--parse-snippet-body (fullname)
  "Return the body of the snippet file content as a string.
This is called by `yas-expand-snippet' for expansion of the snippet.
FULLNAME: should be an already existing file."
  (let ((saved-text
	 (with-temp-buffer
	   (insert-file-contents-literally fullname)
	   (goto-char (point-min))
	   (unless (search-forward-regexp "^[#]\s*--\s*[\n\r]+")
	     (error "Snippet malformed: cannot find the body of the snippet %s after '# --' line" fullname))
	   (setq saved-text
		 (buffer-substring-no-properties
		  (point) (point-max))))))
    saved-text ;;(replace-regexp-in-string "\"" "\\\\\"" saved-text)
    ))


;;; ;; upstream bug by `yasnippet', sometimes snippet
;;; ;; not recognized even correctly identifying its
;;; ;; basename and major-mode and already turn on
;;; ;; `yas-minor-mode' and rerun `yas-reload-all'
;;; (defun h-yas-lookup-snippet (fullname)
;;;   "Wrapper around `yas-lookup-snippet'. This extracts the snippet
;;;   name (defined in its name: field in the snippet header) and the
;;;   major mode of this snippet.  
;;; FULLNAME: the full path of the snippet file."
;;;   (let* ((mode (replace-regexp-in-string
;;; 		     "^.*/\\([^/]+\\)/[^/]+$" "\\1"
;;; 		     fullname))
;;; 	 (case-fold-search t)
;;; 	 (name (with-temp-buffer
;;; 		 (insert-file-contents-literally fullname)
;;; 		 (goto-char (point-min))
;;; 		 (search-forward-regexp "^[#]\s*name:\s+")
;;; 		 (replace-regexp-in-string
;;; 		  "\s+$" ""
;;; 		  (buffer-substring-no-properties
;;; 		   (point) (line-end-position))))))
;;;     (yas-lookup-snippet name (intern mode))))


;;;** Immitate yasnippet behavior when failing to expand:
;;; pretending this minor mode is not active when the tab key is press
;;; -> calling the default command when this minor mode is not active:
(defun h-yas--fallback ()
  "Fallback after expansion has failed.
Common gateway for `yas-expand-from-trigger-key' and
`yas-expand-from-keymap'."
  (let* ((yas-minor-mode nil)
	 (beyond-yas (h-yas--keybinding-beyond-yas)))
    ;; (message "Falling back to %s"  beyond-yas)
    (cl-assert (or (null beyond-yas) (commandp beyond-yas)))
    (setq this-command beyond-yas)
    (when beyond-yas
      (call-interactively beyond-yas))))


(defun h-yas--keybinding-beyond-yas ()
  (let* ((yas-minor-mode nil)
	 (keys (this-single-command-keys)))
    (or (key-binding keys t)
        (key-binding (yas--fallback-translate-input keys) t))))


;;; * KEY
(define-key yas-minor-mode-map [tab] 'h-yas-expand)


(provide 'h-yasnippet)
;;; h-yasnippet.el ends here
